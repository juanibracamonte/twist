twist.js
========

Format-perserving Encryption

## Installation
```bash
$ bower install twist
```
```html
<script type="text/javascript" src="/bower_components/twist/twist.js"></script>
```

## Usage
```javascript
var pattern = /^[A-Za-z0-9]+$/;
var message = '1V456pgvwW17TxUXoKj0DKDvbxnFTgoM';

// Encrypt a message within a pattern:
var encrypted = twist.encrypt(pattern, message, '1234');
// "N7wDUTa5MCxJvfSlI4jGj6FD9lrZ3Wui"

twist.decrypt(pattern, encrypted, '1234');
// "1V456pgvwW17TxUXoKj0DKDvbxnFTgoM"

// Decrypt it with another secret:
twist.decrypt(pattern, encrypted, '5555');
// "rBaRIrYdUuFBNh4x4QfmpmV3ZlRjnqoC"
```
